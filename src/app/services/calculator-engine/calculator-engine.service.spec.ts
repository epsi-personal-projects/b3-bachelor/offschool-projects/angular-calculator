import { TestBed } from '@angular/core/testing';

import { CalculatorEngineService } from './calculator-engine.service';

describe('CalculatorEngineService', () => {
  let service: CalculatorEngineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculatorEngineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
